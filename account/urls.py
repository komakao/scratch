from django.conf.urls import url
from . import views

urlpatterns = [
    # post views
    url(r'^login/$', views.user_login, name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', name='logout'),    
    url(r'^$', views.dashboard, name='dashboard'),
    url(r'^register/$', views.register, name='register'),   
    url(r'^profile/(?P<user_id>\d+)/$', views.profile),    
    url(r'^password-change/$',
        'django.contrib.auth.views.password_change',
        name='password_change'),
    url(r'^password-change/done/$',
        'django.contrib.auth.views.password_change_done',
        name='password_change_done'),    
    url(r'^password/(?P<user_id>\d+)/$', views.password, name='password'),
    url(r'^adminrealname/(?P<user_id>\d+)/$', views.adminrealname, name='adminrealname'),    
    url(r'^realname/$', views.realname, name='realname'),   
    url(r'^log/(?P<kind>\d+)/(?P<user_id>\d+)/$', views.LogListView.as_view(), name='log'),	    
]
